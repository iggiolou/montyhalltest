package domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class Box for Monty Hall Game
 */
public class Box {
    private boolean winner;


    public boolean isWinner() {
        return winner;
    }

    public void setWinner(boolean winner) {
        this.winner = winner;
    }

    //generate boxes and select winner
    public static List<Box> generateBoxes() {

        List<Box> boxes = new ArrayList<>();
        Box box1 = new Box();
        Box box2 = new Box();
        Box box3 = new Box();
        box3.setWinner(true);
        boxes.add(box1);
        boxes.add(box2);
        boxes.add(box3);

        Collections.shuffle(boxes);

        return boxes;


    }

}
