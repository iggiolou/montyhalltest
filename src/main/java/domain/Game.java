package domain;

import java.util.List;
import java.util.Random;

/**
 * Game simulation. The application will take number of times game is played and 50% will change the decisions.
 */
public class Game {

    public void playGame(int time) {

        int winNoChange = 0;
        int winChange = 0;

        Random random = new Random();

            //generate random win box
            List<Box> boxes = Box.generateBoxes();
            //randomly select one box
            Box selected = boxes.get(random.nextInt(3));
            //select another empty box
            Box empty = boxes.stream().filter(x -> !x.isWinner() && !x.equals(selected)).findFirst().get();

            //1-change, 0 - no change
            if (time % 2 == 0) {
                boolean wins = boxes.stream().filter(x -> !x.equals(empty) && !x.equals(selected)).findFirst().get().isWinner();
                if (wins) {
                    winChange += 1;
                }
            } else {
                boolean wins = selected.isWinner();
                if (wins) {
                    winNoChange += 1;
                }

        }

        System.out.println("Switching wins " + winChange + " times.");
        System.out.println("Staying wins " + winNoChange + " times.");

    }
}
