package application;

import domain.Game;

import static util.Util.readTheNumber;

public class MontyHall {

    public static void main(String[] args) {
        System.out.println("Write number of tests:");

        //propose user to select number of cases that will be generated
        int times = readTheNumber();
        //repeat game n times
        for (int index = 0; index < times; times++) {

            new Game().playGame(index);

        }

    }


}
